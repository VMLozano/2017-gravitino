\section{Recasting the experimental searches}
\label{sec:recasting}

We generated benchmark points with the spectrum generator {\tt SPheno\,4.0.0} \cite{Porod:2003um,Porod:2011nf,Staub:2017jnp} using the scanning tool {\tt SSP1.2.3} \cite{Staub:2011dp}. 
The truth level MC events are generated with the help of {\tt Pythia\,8.219} \cite{Ball:2012cx} employing the default parton distribution function (PDF) {\tt NNPDF\,2.3} set \cite{Ball:2012cx} while keeping the default values in the MC tools. We interfaced the truth level events to {\tt CheckMATE\,2.0.2} \cite{Dercks:2016npn,Kim:2015wza,Drees:2013wra,Tattersall:2016nnx} which is based on the fast detector simulation {\tt Delphes\,3.4.0} \cite{deFavereau:2013fsa} and the jet reconstruction tool {\tt Fastjet\,3.2.1} \cite{Cacciari:2011ma}. {\tt CheckMATE} tests a model point against current ATLAS and CMS searches at $95\%$ confidence level.\footnote{{\tt CheckMATE} contains a number of high luminosity searches which allows the user to estimate the potential discovery reach, e.g. \cite{Kim:2016rsd}. In addition, with the help of the AnalysisManager new search strategies can be implemented \cite{Rolbiecki:2015lsa,Kim:2014noa}.} We take the leading order cross section from {\tt Pythia\,8} and normalise it with a conservative $\mathcal{K}$ factor of 20\%. We do not generate MC events with at least one additional parton at matrix element level and matched with the $p_T$ ordered parton shower of {\tt Pythia\,8}. In principle, our approach might be problematic for compressed spectra \cite{Dreiner:2012gx,Dreiner:2012sh,Drees:2012dd}. However, in this letter the effects are negligible, since the decay of the NLSP into the gravitino will dominate the final state topology.

We have performed several grid scans in the softbreaking parameters of the electroweakino sector as well as $\mu$ with the SM-like Higgs mass fixed to $m_h=125$ GeV. In Table~\ref{tab:scan_parameters}, we list all scenarios and the scan parameters. They will be discussed in more detail further below. For each grid point, $5\cdot 10^4$ events have been generated.

We test each grid point against all relevant ATLAS and CMS Run-1 searches implemented into {\tt CheckMATE}. The most relevant searches of which are summarised in Table \ref{tab:lhc_searches8tev}. 
The implementation of Ref.~\cite{Aad:2015hea} has been done for this project and is now publicly available within the standard {\tt CheckMATE} repository.
All listed analyses are fully validated, and validation informations are provided on the official webpage\footnote{\url{https://checkmate.hepforge.org/}} for the interested reader.

Every analysis covers a large number of signal regions which provides sensitivity to a vast range of mass hierarchies and final state multiplicities. For each search, the {\it best signal region} corresponds to the signal region with the best {\it expected} exclusion potential. This approach is again followed to choose the {\it best search}. As a result, the best observed limit is not always used to determine the limit and thus naively, the limits might be weaker. However, on the other hand, our limits are insensitive to downward fluctuations in the data which we would expect taking into account the huge number of signal regions. We select the {\it best search} and then compare our estimate of number of signal events with the observed limit at 95\% confidence level \cite{Read:2002hq},

\begin{equation}
r=\frac{S-1.96\cdot\Delta S}{S_{\rm exp}^{95}}\,,
\label{eq:r}
\end{equation}

where $S$, $\Delta S$, and $S_{\rm exp}^{95}$ denotes the number of signal events, the uncertainty due to MC errors and the 95$\%$ confidence level limit on the number of signal events, respectively. We only consider the statistical uncertainty due to the finite Monte Carlo sample with $\Delta S=\sqrt{S}$. The $r$ value is only calculated for the expected best signal region. 
Note that a combination of signal regions is currently not yet possible in {\tt CheckMATE}.
A model point is excluded if $r$ is larger than one. However, here we define a point as allowed for $r<0.67$ and excluded for $r>1.5$. Benchmark points which fall in the region $0.67<r<1.5$ might be excluded or allowed but due to missing higher order corrections and other systematic errors, we do not classify them as excluded or allowed. 


\begin{table}[tbh]
\begin{center}
\begin{tabular}{l|l|l}
Reference & Final State & $\mathcal{L}$ [fb$^{-1}$]\\
\hline
\hline
1507.05493 \cite{Aad:2015hea} & $1(2)\gamma+0(1)\ell$+(b)-jets+$\met$ & 20.3\\
1411.1559 (ATLAS) \cite{Aad:2014tda} & $\gamma+\met$ & 20.3 \\
1501.07110 (ATLAS) \cite{Aad:2015jqa} & $h+\met$ & 20.3 \\
1403.4853 (ATLAS) \cite{Aad:2014qaa} & 2$\ell$+$\met$& 20.3 \\
1404.2500 (ATLAS) \cite{Aad:2014pda} & SS 2$\ell$ or 3$\ell$ & 20.3\\
1405.7875 (ATLAS) \cite{Aad:2014wea} & jets + $\met$ & 20.3\\
1407.0583 (ATLAS) \cite{Aad:2014kra} & 1$\ell$+($b$) jets+$\met$ & 20.0\\
1402.7029 (ATLAS)  \cite{Aad:2014nua} & 3$\ell$+$\met$& 20.3 \\
1501.03555 (ATLAS) \cite{Aad:2015mia} & 1$\ell$+jets+$\met$ & 20.3\\
1405.7570 (CMS) \cite{Khachatryan:2014qwa} & 1,\,SS-OS2,\,3,\,4$\ell$+$\met$& 20.3 \\
\end{tabular}
\end{center}
\caption{Summary of the most relevant analyses. In addition, we have also taken into account all other searches implemented in {\tt CheckMATE} for our study.
The 8 TeV analyses are referenced by their arXiv number. The middle column denotes the final
state topology, and the third column shows the total integrated luminosity.}
\label{tab:lhc_searches8tev}
\end{table}

Before presenting our numerical results, we first want to provide a rough overview of the relevant Run 1 and Run 2 searches and their current status. ATLAS and CMS have presented a vast number of studies covering a large selection of final state topologies. We want to investigate the impact on our GGM scenario by taking into account all relevant 8 TeV and 13 TeV searches since only a few dedicated GGM studies have been on the market so far. In Table \ref{tab:lhc_searches8tev} we list all relevant searches implemented in {\tt CheckMATE}. The search \cite{Aad:2015hea} is a tailor made study targeting GGM inspired models and provides the backbone of our suit of LHC analyses. The authors search for events with high energetic isolated photons and large transverse momentum in the full data set of Run 1. The analysis contains ten signal regions which can roughly be divided into four classes. The first class demands diphoton final states with large transverse momentum targeting gluino and wino pair production channels with subsequent decays into binolike NLSPs. The remaining signal regions only require a single high energetic photon in the event topology which are further classified in ($b$)-jet multiplicity signal regions. This class of search channels probes scenarios with gluinos and higgsinos and subsequent cascade decays into bino NLSPs. Finally, signal regions requiring an isolated electron or muon in the final state are designed to focus on wino NLSP scenarios. Ref.~\cite{Aad:2014tda} is designed to look for events with a single large transverse momentum photon balancing large missing transverse energy. 
The search targets models of simplified dark matter where pair-produced dark matter recoils against a photon of SUSY-inspired scenarios with compressed spectra where pair produced squarks are produced in association with a photon. The SM backgrounds of the mono-photon signal can be suppressed by demanding a high momentum photon and large missing transverse momentum while requiring a strict lepton veto. As we will see, Ref.~\cite{Aad:2015jqa} performs pretty well in certain regions of parameter space. This analysis searches for chargino--neutralino pairs decaying into a $W$ boson and Higgs boson final state. Here, the authors exploit the emerging lepton of the $W$ boson decay as well as the diphoton, bottom pairs and the $WW^*$ final state of the Higgs decay to isolate the signal from the countless SM events. We also employed many other SUSY searches which might be sensitive to our GGM scenario such as multi--lepton searches \cite{Aad:2014qaa,Aad:2014nua} and studies covering multi jet plus lepton final states \cite{Aad:2014kra}. Finally, we want to discuss the existing 13 TeV searches. The number of existing 13 TeV analyses is quite modest at the moment and are designed to maximise the sensitivity in the high mass region. In particular, 13 TeV GGM focused searches are quite rare. Ref.~\cite{ATLASCollaboration:2016wlb} searches for two photons and large transverse momentum and they interpret their results in GGM models. Its signal regions resemble the diphoton signal regions of the 8 TeV study \cite{Aad:2015hea} aiming at very heavy gluinos cascade decaying into diphoton final states. Due to the heavy masses involved, they can demand a very tight cut on the effective mass of $m_{\rm eff}>1.5~$TeV to separate the diphoton signal from the large SM backgrounds. This analysis is therefore not sensitive to the comparably small cross sections we are dealing with here. The other search is a conference note~\cite{ATLAS:2016fks} looking for photons, jets and large missing transverse momentum in the final state. Again, they demand a very large cut $m_{\rm eff}>2~$TeV. As a consequence only 8 TeV analyses are relevant for us and we will proceed with the 8 TeV searches and present our numerical results in the next section.
