\section{Numerical Results}
\label{sec:results}

In order to study the phenomenology of the GGM models we focus on a set of scenarios reflecting all possible mass hierarchies in the electroweakino sector while assuming that the gravitino is always the LSP. As discussed before we have two input parameters in the gaugino soft breaking sector namely $M_1$ and $M_2$ as well as $\mu$ and $\tan\beta$. We consider scenarios with bino, wino, higgsino NLSP scenarios as well as mixed eigenstates NLSP candidates. We explicitly checked that our limits on the model parameters are barely sensitive to the exact value of $\tan\beta$ and thus we fix $\tan\beta=10$ in the remainder of our letter. In the case where differences appear for other choices, we discuss them and show the respective results. In the following, we perform several two-dimensional grid scans while fixing the remaining input parameters. We have chosen four scenarios which are summarised in Table \ref{tab:scan_parameters}. These four scenarios are sufficient to consider all possible mass orderings in the electroweakino sector with a gravitino LSP. In our first scenario {\bf I} we concentrate on light binos and winos while decoupling the higgsino sector. Since the production cross section for purely bino-like eigenstates is negligible, the signal rate is governed by the production channels of electroweakinos with large wino composition. Analogously in scenario {\bf II}, we consider light binos and higgsinos while the winos are decoupled. Similar to scenario {\bf I} only higgsino-like final states have a sufficiently large cross section which can be probed at the LHC. In scenario {\bf III}, we take a closer look at scenarios with simultaneous presence of light higgsinos and winos. In addition, we assume a light bino. The presence of a light bino with a mass of 50 GeV in the mass spectrum warrants a bino NLSP. Finally, in scenario {\bf IV} we consider higgsinos and winos which can be kinematically accessed at the LHC. However, we factor out the impact of the bino on the collider phenomenology by setting its mass to 2 TeV.

\begin{table}
\centering
\begin{tabular}{c|c|c|c|c} %\hline \hline %\toprule
Scenario & $M_1$ [GeV] & $M_2$ [GeV] & $\mu$ [GeV] & Description \\ \hline\hline
{\bf I} & [100, 1000] & [100, 1000] & 2000 & $\mu$ decoupled \\
{\bf II} & [100, 1000]  & 2000 & [100, 1000] & $M_2$ decoupled\\
{\bf IIb} & [100, 1000]  & 2000 & [-1000, -100] & $M_2$ decoupled\\
{\bf III} & 50 & [100, 1000] & [100, 1000] & light bino\\
{\bf IV} & 2000 & [100, 700] & [100, 700] & heavy bino
%\\ \hline\hline %\bottomrule
\end{tabular}
\caption{For all scenarios, we effectively decouple the scalar sector, the gluinos as well as pseudoscalars from the spectrum while keeping the Higgs mass to 125 GeV. We use $\tan\beta=10$ throughout except for scenario {\bf IIb} where we set $\tan\beta=3$.
}\label{tab:scan_parameters}
\end{table}

\subsection*{Scenario {\bf I}}

In Fig.~\ref{fig:anal_mudecoupled}, we show the best exclusion limit at 95\% confidence level denoted by the black dashed curve in the  $M_1$--$M_2$ plane while $\mu$ is fixed to 2 TeV. The dark gray shaded area corresponds to our theoretical uncertainty while the light gray one represents the excluded region. We also present the exclusion potential of the three  most important searches which are shown in green \cite{Aad:2015hea}, blue \cite{Aad:2014tda} and red \cite{Aad:2015jqa}. 
Ref.~\cite{Aad:2015hea} provides the best sensitivity which is hardly surprising since this search is targeting GGM scenarios with bino and wino NLSPs. In particular, the signal regions optimised to the diphoton states are powerful. Here, the kinematical selection cuts are optimized to isolate events with wino production channels with subsequent decays into binos. They demand at least two photons with $p_T\ge75$ GeV each and $\met\ge150\,(200)$ GeV for low (high) mass bino NLSPs. The latter cut allows to be sensitive to compressed mass spectra as well as to scenarios with large mass splitting between the wino and the bino NLSP. Our results nicely agree with the results in Ref.~\cite{Aad:2015hea}. The experimental analysis presents mass limits in the wino--bino mass plane with a bino NLSP and their limit is relatively insensitive to the mass difference between wino and bino with the upper mass limit of roughly 700 GeV for the wino eigenstates which we are able to reproduce within the systematic uncertainty. Moreover, they present limits for a wino NLSP scenario. The most sensitive signal region requires at least one photon with $p_T\ge125$ GeV, $\met\ge120$ GeV and the presence of an isolated electron or muon. Here, they derive a limit of roughly 350 GeV on the wino mass. Again, we agree with their results within the theoretical uncertainty. The main contribution to the relevant signal comes from a production of a wino-like neutralino together with a wino-like chargino. While the photon comes from the neutralino decay, the isolated lepton comes from the direct chargino decay $\tilde \chi^\pm_1 \to W^\pm \tilde G$ and the corresponding leptonic $W$ decay.
Fig.~\ref{fig:anal_mudecoupled} demonstrates that also Ref.~\cite{Aad:2014tda} performs very well in the wino NLSP region. The kinematical cuts on the transverse momentum of the photon and the missing transverse momentum are similar. However, they demand a lepton veto and thus no further cuts on kinematic quantities requiring a lepton are demanded. Nevertheless, similar limits are obtained compared to the GGM search of Ref.~\cite{Aad:2015hea}. 
Moreover, while Ref.~\cite{Aad:2015hea} becomes insensitive to low $M_2$ values  (see the green vertical line at $M_2\simeq 120~$GeV), the usage of Ref.~\cite{Aad:2014tda} enables us to exclude also this low-mass region with LHC data.


\begin{figure}[htbp]
	\begin{center}
        \includegraphics[width=.85\linewidth]{figs/analysis_mudecoupled.pdf}
	\end{center}
\caption{95\% confidence level limit on the $M_1$ -- $M_2$ plane from ATLAS and CMS searches performed at the center of mass energy $\sqrt{s}=8$ TeV. The green, blue and red lines correspond to 95\% C.L. limits from recasting the analyses of Refs.~\cite{Aad:2015hea}, \cite{Aad:2014tda} and \cite{Aad:2015jqa}. %The black curve corresponds to the envelope around the best exclusion limit. The light gray area is excluded from these analyses while the white area is still allowed parameter space.
For the best exclusion line, we show in dark gray the theoretical uncertainty bands. 
We also show in black dashed the best exclusion line as provided by {\tt CheckMATE} when considering all implemented searches. Due to the slightly different approach in the limit setting explained in \cref{sec:recasting}, there can appear small differences with respect to the envelope of the coloured exclusion lines.
Here we decoupled the higgsino sector by fixing $\mu=2~$TeV. }
\label{fig:anal_mudecoupled}
\end{figure}

\subsection*{Scenario {\bf II}}
We present our limits of scenario {\bf II} in Fig.~\ref{fig:anal_m2decoupled}. The shape of the excluded region is very similar to scenario {\bf I} for $\mu\ge300$ GeV. The diphoton signal regions in Ref.~\cite{Aad:2015hea} are relatively insensitive to the details of the decay products of the heavy neutralino states into the lightest neutralino and thus similar efficiencies in the signal regions are expected for wino and higgsino induced diphoton final states. The upper limit on the higgsino mass might be somewhat surprising since the production cross section for higgsino mass eigenstates are considerably smaller than for wino eigenstates. On the other hand, we have two higgsino eigenstates which are close in mass and therefore compensate for the smaller production cross section.
Benchmark points with higgsino NLSPs in the region $\mu\le300$ GeV and $M_1\ge300$ GeV are very weakly constrained by Run 1 data. We can clearly see that the limit on the higgsino mass parameter are substantially smaller than in the wino NLSP case with a limit very close to the LEP II bound \cite{Agashe:2014kda}.  In the higgsino NLSP region,  the decay $\tilde \chi^0_1 \to \tilde G \gamma$ is only possible through the small bino admixture present in the NLSP state. Consequently, as soon as the bino mass is considerably larger than $\mu$, the photonic decay
is suppressed by the much larger branching ratio into $\tilde G Z$ and $\tilde G h$. Therefore,whenever the decay into $\tilde G h$ is kinematically accessible, BR($\tilde \chi^0_1 \to \tilde G \gamma$) ranges below a per-cent, making the signal efficiency in the diphoton signal regions negligible. 
There are, however, two effects through which the single-photon searches of Refs.~\cite{Aad:2015hea,Aad:2014tda} become sensitive at least in the small-$\mu$ region. Firstly, 
%\MKout{However,} 
due to the small mass splitting between the lighter and the next heavier higgsino state, the %\MKout{two-body} 
decay of $\tilde \chi^0_2$ down to the NLSP 
%\MKout{into $\tilde \chi^0_1 \gamma$ is typically at the level of a few per-cent and} 
is mainly into a three-body final state %\MKout{otherwise} 
but also a significant fraction of typically a few per-cent decays into $\tilde \chi^0_1 \gamma$. 
Secondly, for small enough $\mu$, the decay mode $\tilde G h$ becomes kinematically suppressed, leading to a relative enhancement of the photonic mode $\tilde G \gamma$.
%\MKout{Through this extra photon from the NNLSP decay, the single photon signal regions of Refs.~\cite{Aad:2015hea,Aad:2014tda} are sensitive in a small window with low higgsino masses $\mu \lesssim 150~$GeV.}
We further see that, with increasing $M_1$, corresponding to a decreasing bino admixture within $\tilde \chi^0_1$, the experimental searches become insensitive to the higgsino NLSP scenario accordingly. 
In the region with both low $\mu$ and low $M_1$ where the NLSP is made of a significant bino-higgsino mixture, the mono-photon search of Ref.~\cite{Aad:2014tda} is actually providing the best limits. In this region, the NLSP decay into $\tilde G \gamma$ ranges around 10\,\% so that the analysis is quite sensitive to a pair of neutralinos where one decays into $\tilde G \gamma$ and the other into $\tilde G$ and two jets or two neutrinos from the $Z/h$ decay.

\begin{figure}[htbp]
	\begin{center}
        \includegraphics[width=.85\linewidth]{figs/analysis_m2decoupled.pdf}
	\end{center}
\caption{Same as in \cref{fig:anal_mudecoupled} but in the $M_1$ -- $\mu$ plane using $M_2=2~$TeV.}
\label{fig:anal_m2decoupled}
\end{figure}
\begin{figure}[htbp]
	\begin{center}
        \includegraphics[width=.85\linewidth]{figs/analysis_m2decoupledtb3neg.pdf}
	\end{center}
\caption{Analogue of \cref{fig:anal_m2decoupled} but using $\tan\beta =3$ and $\mu<0$.}
\label{fig:anal_m2decoupledtb3neg}
\end{figure}

Finally note also that the decay properties of the higgsino are quite sensitive to the exact value of $\tan \beta$ and also the sign of $\mu$: if $\tan\beta$ is small, then using \cref{eq:kappas}, the partial decay width of the higgsino into $h\tilde G$ is enhanced if $\mu<0$. Correspondingly, both the branching ratios into $\tilde G Z$ and $\tilde G \gamma$ are suppressed. 
This scenario is depicted in \cref{fig:anal_m2decoupledtb3neg} where we repeat the scan of \cref{fig:anal_m2decoupled} with $\tan\beta=3$ and $\mu<0$ while keeping the Higgs mass at the observed value. One can clearly see that the above-mentioned region with significant higgsino-bino mixing is less constrained by existing searches, in particular Ref.~\cite{Aad:2014tda} looses sensitivity. However, in the narrow band of small $\mu$ but large $M_1$, there are only very small differences in the exclusion bounds with respect to \cref{fig:anal_m2decoupled}.
%, which is due to the kinematical suppression of the $\tilde G h$ decay mode. 
The reason here is simply that the lightest higgsino is slightly lighter for $\mu<0$ than for $\mu>0$, so that the region of kinematical suppression of the $\tilde \chi^0_1 \to \tilde G h$ decay is already reached for slightly higher values of $\mu$.





\subsection*{Scenario III}

Now, we investigate how mixed wino--higgsino states can be constrained by Run-1 data. Fig.~\ref{fig:anal_lightbino} presents the excluded region in the $M_2$--$\mu$ plane. Here, we fix the bino mass to 50 GeV. The excluded region extends to roughly 600~GeV for pure higgsinos, winos as well as for mixed states. Our result is consistent with Fig.~\ref{fig:anal_mudecoupled} and \ref{fig:anal_m2decoupled} of scenarios {\bf I} and {\bf II}. In the previous two subsections, we showed that the upper mass limit for higgsinos and winos with a bino NLSP is almost independent of the mass splitting since the search~\cite{Aad:2015hea} has two signal regions covering the low and high mass neutralino LSP range. We can observe that, as also seen in \cref{fig:anal_mudecoupled}, the search~\cite{Aad:2015hea} looses sensitivity in the very low mass region $M_2\approx 200~$GeV or $\mu \approx 200~$GeV since the signal regions are optimized for heavier masses. However, Ref.~\cite{Aad:2015jqa} shows sensitivity in the respective regions and is able to exclude the remaining parameter space for low $M_2$ and $\mu$. This search is optimized for neutralino--chargino pair production with the heavy neutralino decaying into a Higgs boson. In particular, the diphoton signal regions are very sensitive in this region of parameter space. The selections cuts require an isolated lepton and two photons with $p_T\ge25,35$ GeV. In addition, they demand a moderate cut on missing transverse momentum of 40 GeV. Finally, they apply cuts to enhance sensitivity to leptonically decaying $W$ bosons.

%\begin{figure*}[htbp]
\begin{figure}[h]
	\begin{center}
          % \includegraphics[width=.425\linewidth]{figs/analysis_lightbinofixed.pdf}
           \includegraphics[width=.85\linewidth]{figs/analysis_lightbinofixed.pdf}
	\end{center}
\caption{Same as in \cref{fig:anal_mudecoupled} but in the $M_2$ -- $\mu$ plane and $M_1=50~$GeV.
Here we show in black also the LEP~II limit which excludes chargino masses below 104~GeV \cite{Agashe:2014kda}.}
\label{fig:anal_lightbino}
\end{figure}


\subsection*{Scenario IV}

In Fig.~\ref{fig:anal_heavybino}, we present the limits in the $M_2$ -- $\mu$ plane while decoupling the bino from the electroweakino mass spectrum. As expected, the limits are substantially weaker with the absence of the bino NLSP since the main  source of generating isolated photons is lost. Instead, while the wino still decays into $\tilde G\gamma$, cf. \cref{eq:partialwidths,eq:kappas}, the higgsinos decay into $\tilde G Z/h$ as also in the upper left corner of \cref{fig:anal_m2decoupled}.
The shape of the excluded region can easily be understood from our discussion of scenarios {\bf I} and {\bf II} and the results summarised in Figs.~\ref{fig:anal_mudecoupled} and \ref{fig:anal_m2decoupled}. We can read off the limits for decoupled higgsinos or winos in the region where the bino is not the NLSP candidate. Analogously to \cref{fig:anal_mudecoupled}, a wino NLSP is excluded up to $\sim 300~$GeV. In the wino decoupled scenario, we observe that $\mu\lesssim 130~$GeV is excluded, similar to \cref{fig:anal_m2decoupled}. 
Moreover, we can see that in the bino decoupled scenario, the GGM search \cite{Aad:2015hea} as well as the DM/compressed SUSY search \cite{Aad:2014tda} have comparable sensitivity while the electroweakino search \cite{Aad:2015jqa} covers the low mass region in the lower left corner of Fig.~\ref{fig:anal_heavybino}. 
As is also seen in scenario~{\bf{II}} in the mixed higgsino-bino region, the mixed higgsino-wino region in \cref{fig:anal_heavybino} is also best covered by the non-dedicated mono-photon search \cite{Aad:2014tda}.
%To summarise, SUSY searches not optimized for GGM scenarios can have similar sensitivity and even cover regions of parameter region missed by GGM searches. Our results clearly show the complementarity of searches.

%\begin{figure}[htbp]
\begin{figure}[ht!]
	\begin{center}
        \includegraphics[width=.85\linewidth]{figs/analysis_heavybino.pdf}
	\end{center}
\caption{Same as in \cref{fig:anal_lightbino} but using $M_1=2~$TeV.}
\label{fig:anal_heavybino}
\end{figure}
