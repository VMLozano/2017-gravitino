\section{General Gauge Mediation Models}
\label{sec:model}

In the minimal GMSB scenario, %\footnote{For an overview, see Refs.~\cite{Giudice:1998bp,Martin:1996zb} and references therein.} 
the spectrum of supersymmetric particles is determined by their respective quantum numbers along with SUSY breaking scale and the
properties of the messenger sector. Typically one assumes the messenger fields to form complete multiplets under SU(5) so that gauge coupling unification is trivially preserved.
% multiplicity, representation and mass scale $M$ of the messenger fields as well as the scale of SUSY breaking.
%  Once these parameters together with the 
Once the messenger sector and the  
  ratio of the up- and down-type Higgs vevs $\tan\beta=v_u/v_d$ are fixed, the hierarchy of the spectrum is determined. In particular, the same messenger fields which fix the masses of the scalar particles also determine the gaugino spectrum, with the fixed ratio of bino, wino and gluino masses of $M_1:M_2:M_3 = g_1^2:g_2^2:g_3^2$ at the messenger scale $M$.\footnote{Note that this relation uses the GUT-normalized U(1) gauge coupling $g_1$.}

In general gauge mediation models, in turn, a more general messenger sector is considered and
interactions between matter and messenger fields are included. 
Consequently, the spectra of GGM models can differ quite significantly from minimal GMSB models, although
some features such as small softbreaking trilinear couplings ($A$-terms) and sum rules for soft SUSY-breaking masses remain \cite{Meade:2008wd}. In addition, the gravitino typically remains the LSP.

The MSSM gaugino and sfermion soft masses can be written, to leading order, as \cite{Buican:2008ws}
\begin{equation}
M_k = g_k^2 M B_k\quad {\rm and} \quad m_f^2 = \sum_{k=1}^3 g_k^4 C_k(f) A_k\,,
\end{equation} 
where $k$ labels the gauge groups, $f$ the MSSM matter representations and  $C_k (f)$ is the quadratic Casimir invariant of the scalar field $f$ with respect to the gauge group $k$. In Ref.~\cite{Buican:2008ws}, it was shown that 
one can construct weakly-coupled messenger theories in such a way that the complete parameter space of the parameters $B_k$ and $A_k$ is covered.\footnote{See also Refs.~\cite{Rajaraman:2009ga,Komargodski:2008ax} for an exploration of the GGM parameter space.} 
In addition to $A_k$ and $B_k$, the remaining free parameters at the weak scale are the higgsino mass term $\mu$ as well as $\tan \beta$. 
Consequently, the hierarchy among the electroweakinos is undetermined. Moreover, any one of them could be the NLSP with masses well below a TeV while having at the same time large enough $A_k$ such that the stops are in the multi-TeV range as required from the Higgs mass constraint in conjunction with small $A$-terms.\footnote{This problem can, however, be alleviated by certain types of messenger-matter interactions \cite{Grajek:2013ola,Byakti:2013ti,Evans:2013kxa,Craig:2012xp,Abdullah:2012tq,Albaid:2012qk,Evans:2012hg,Evans:2011bea,
Shadmi:2011hs} or extra tree-level contributions to the Higgs mass \cite{Krauss:2013jva}.}
 If also the rest of the scalar sector and the gluinos are heavy, the first (and possibly the only) SUSY particles accessible at the LHC would be the electroweakinos.

In the following, we will investigate this scenario, treating $M_1$, $M_2$, $\mu$ and $\tan \beta$ as free parameters.
We further assume that the sfermions as well as the gluino are heavy enough to not (yet) be accessible at the LHC in the near future. 

Once produced, the NLSP will undergo the decay $\tilde \chi^0_1 \to \tilde G \gamma/Z/h$, depending on the NLSP nature.
The partial decay width into $\tilde G \gamma$ 
is for instance given by \cite{Ambrosanio:1996jn}
\begin{equation}
\Gamma_{\tilde G \gamma} = 1.12 \cdot 10^{-11}~{\rm GeV} \, \kappa_\gamma \left( \frac{m_{\tilde \chi^0_1}}{100~{\rm GeV}} \right)^5 \left( \frac{m_{\tilde G}}{1~{\rm eV}} \right)^{-2}\,,
\end{equation}
and the corresponding ratios of partial widths can be approximated as~\footnote{These relations do not hold near threshold, i.e. when for instance $m_{\tilde \chi^0_1}-(M_Z+m_{\tilde G})$ is small.} 
\begin{align}
\label{eq:partialwidths}
\frac{\Gamma_{ h \tilde G}}{\Gamma_{\gamma \tilde G}} = \frac{\kappa_h}{\kappa_\gamma} \left(1-\frac{m_h^2}{m^2_{\tilde \chi^0_1}} \right)^4\,,\quad
\frac{\Gamma_{ Z \tilde G}}{\Gamma_{\gamma \tilde G}} &= \frac{\kappa_Z}{\kappa_\gamma} \left(1-\frac{M_Z^2}{m^2_{\tilde \chi^0_1}} \right)^4 \,,
\end{align}
where
\begin{align}
\label{eq:kappas}
\nonumber \kappa_\gamma &= |N_{11} c_{W} + N_{12}s_W|^2,\,\quad\kappa_h = |N_{13}s_\alpha - N_{14} c_\alpha|^2,\,\\
\kappa_Z &= |N_{11} s_W-N_{12}c_W|^2 + \frac{1}{2} |N_{13} c_\beta-N_{14}s_\beta|^2\,.
\end{align}
Here we have used $c_\phi = \cos \phi\,,s_\phi=\sin \phi$, subscript $W$ refers to the weak mixing angle and $\alpha$ is the  mixing angle in the CP-even Higgs sector. $N_{ij}$ is the neutralino mixing matrix in the basis $(\tilde B,\tilde W^0,\tilde H^0_d,\tilde H^0_u)$.
% Complete expressions for the neutralino two-body decay into $\tilde G \gamma/Z/h$ can be found in Ref.~\cite{Giudice:1998bp}. If the NLSP is a bino, the decay $\tilde \chi^0_1 \to \gamma \tilde G$ always dominates.  
Since $\kappa_Z/\kappa_\gamma \simeq 0.3$, a bino NLSP will predominantly decay into $\tilde G \gamma$ while a pure wino NLSP mostly decays into $\tilde G Z$ as long as $(M_Z/m_{\tilde \chi^0_1})^2\ll 1$. For a pure higgsino state, the photonic mode is absent and so it decays into both $\tilde G Z$ or $\tilde G h$, with the ratio depending on the model parameters.

We will in the following restrict ourselves to prompt neutralino decays~\footnote{For discussions on long-lived neutralinos, see e.g. Refs.~\cite{Feng:2010ij,Dreiner:2011fp} and Ref.~\cite{Aad:2014gfa} for an experimental search for displaced vertices motivated by long-lived neutralino scenarios within GMSB.}
 which we assure by fixing $m_{\tilde G}=1$~eV in the remainder of this letter. In this case the gravitino is still a good candidate for DM, however it should coexist with other different candidate in order to account for the total relic density~\cite{Viel:2005qj}.
Note however that the exact gravitino mass value is irrelevant for the subsequent study due to $m_{\tilde \chi^0_1}\gg m_{\tilde G}$.



