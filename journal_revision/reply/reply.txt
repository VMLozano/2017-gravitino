Dear editor,

we thank the referee for his/her valuable comments and want to excuse the long time it took us to revise the manuscript.
In the following, we address the two points of criticism raised.

1) In CheckMATE, we neither combine the signal regions of the same analysis nor over all analyses since this would require the knowledge of all correlations which have not been fully published by the experimental collaborations (yet). In any case, our approach is conservative since our derived limit cannot become stronger than the combination of all relevant searches. In addition, in CheckMATE we choose the best expected signal region of all considered searches and compare with the actual observed S95 limit (we do not calculate the CL’s here) and this makes our limit even more conservative. Moreover, we see that in large regions of parameter space a single search is the dominant and most sensitive one so that a combination would not improve the limit significantly. Finally, in regions of parameter space where the sensitivity of let say two searches are similar, the statistics can be increased by a factor of 2 assuming that both searches can be regarded as independent, i.e. we assume that the integrated luminosity is doubled and that the systematic errors remain constant. Here, a combination would improve the limits but the increase of the significance would only be given by rougly a factor of square-root of 2 and the improved mass limit is quite modest since the hadronic cross section falls off exponentially with increasing mass scale.

In section III, we have extended the sentence 
"Note that a combination of signal regions is currently not yet possible in CheckMATE." by
", therefore the limits we derive in the following are conservative"

2) We never tried to hide the fact that we used 8 TeV analyses. Instead, we explained at some length in section III why the 13 TeV ATLAS analyses available at the time are not relevant. 
However, the referee is completely right that we had actually missed two GMSB analyses performed by CMS. Those are CMS-PAS-SUS-16-044 (which was updated as arXiv:1709.04896) and CMS-PAS-SUS-16-046. For the revised version of this work, we have implemented the latter of the two searches into CheckMATE and validated it. As it is technically impossible to produce a reasonable implementation of the former search into CheckMATE (see the explanation below), we have estimated the bounds from it by a rescaling with the actual cross sections we obtain. Furthermore, we include four other ATLAS searches -- two of which looking for a dark matter-inspired photon + MET signal, and two for 2-3 leptons.

Here we now explain in more depth why the implementation of CMS-PAS-SUS-16-044 fails: the analysis looks for the final state of two Higgs bosons and large missing transverse momentum in GMSB-motivated higgsino pair production. It considers a scenario where both Higgses decays into b quarks and thus the search demands 3 to 4 b-jets in the final state. In principle, it is straightforward to implement this search. Unfortunately, no tuned b-tagging efficiency curve for the CMS detector has been implemented so far in CheckMATE. As a consequence we implemented the b-tagging efficiences given in the references of CMS-PAS-SUS-16-044. However, the CMS study considers several working points which have very different shapes of the efficiency curves. In the current version of CheckMATE we cannot implement several b-tagging efficiency curves, so we had to take the average effiency curve for several working points. Unfortunately, the final result was not very satisfactory and we have decided to estimate the sensitivity of CMS-PAS-SUS-16-044 by rescaling the cross sections by the branching ratios of the higgsinos into Higgs bosons and comparing them to the unfolded limits on higgsino production as given in the analysis.

In addition to those analyses, we now also consider all other 13 TeV analyses which are implemented in CheckMATE. However we find that apart from those mentioned above, no other search is sensitive.

Accordingly, we have updated the corresponding parts of the manuscript:
 - In Table I, we have added the short description of these analyses.
 - In the body text of Section III, we have divided the description of the recasted analyses into the subsections "8 TeV analyses" and "13 TeV analyses". In the latter, there is now a short description of the new relevant analyses. Here we also explain differences between the simplifying assumptions made in the 13 TeV GMSB analyses and a realistic model as we consider it.
 - We have updated the results in the Figures to include a recast of the 13 TeV analyses. In addition to the exclusion lines of the 8 TeV searches, we show in dashed lines the limits from the four most constraining 13 TeV analyses. Fig. 3 is the only scenario where the mentioned di-Higgs search CMS-PAS-SUS-16-044 (1709.04896) is sensitive. We have accordingly added the projected limits.
 - We describe and explain in the body text of section IV the respective results from the inclusion of the 13 TeV analyses. We find that the only 13 TeV which significantly adds to the exclusion power of the 8 TeV analyses is the multilepton search ATLAS-CONF-2017-039 which is sensitive to wino and higgsino NLSP scenarios with mases around 300 GeV. 
 - We also added a comment on the sensitivity of 13 TeV multilepton analyses in the last sentence of the introduction as well as the next-to-last sentence of the conclusions.  


We hope that the revised version is suitable for publication in PLB. 

